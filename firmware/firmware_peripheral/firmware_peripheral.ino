#include <NewPing.h>
#include <SPI.h>

//#define DEBUG

//Numerical constants
const int max_distance = 300; //max distance measured before timeout
const int horiz_threshold = 100; //distance in cm to send warning to main mcu
const int vertFront_threshold = 10; //distance in cm to freeze motors
const int vertBack_threshold = 14; //distance in cm to freeze motors
const int confirmationDelay = 50; //delay between measurements when a cliff or obstacle is found
const int readingDelay = 40; //delay between polling each sensor

//Pins
const int frontH_pin = 4;
const int frontV_pin = 3;
const int backH_pin = 5;
const int backV_pin = 6;
const int aux1H_pin = 7; //Placeholder. Code not implemented
const int aux1V_pin = 8; //Placeholder. Code not implemented
const int aux2V_pin = 9; //Placeholder. Code not implemented
const int aux2H_pin = 10; //Placeholder. Code not implemented
const int motorEstop_pin = 2;

//Enable or disable sensors
const boolean frontH_en = true;
const boolean frontV_en = true;
const boolean backH_en = true;
const boolean backV_en = true;
const boolean aux1H_en = false; //Placeholder. Code not implemented
const boolean aux1V_en = false; //Placeholder. Code not implemented
const boolean aux2V_en = false; //Placeholder. Code not implemented
const boolean aux2H_en = false; //Placeholder. Code not implemented

//Objects
NewPing ping_frontH(frontH_pin, frontH_pin, max_distance);
NewPing ping_frontV(frontV_pin, frontV_pin, max_distance);
NewPing ping_backH(backH_pin, backH_pin, max_distance);
NewPing ping_backV(backV_pin, backV_pin, max_distance);
NewPing ping_aux1H(aux1H_pin, aux1H_pin, max_distance); //Placeholder. Code not implemented
NewPing ping_aux1V(aux1V_pin, aux1V_pin, max_distance); //Placeholder. Code not implemented
NewPing ping_aux2V(aux2V_pin, aux2V_pin, max_distance); //Placeholder. Code not implemented
NewPing ping_aux2H(aux2H_pin, aux2H_pin, max_distance); //Placeholder. Code not implemented





void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(motorEstop_pin, INPUT); //Input to let the other uP to drive as well
  
  SPI.begin();
  digitalWrite(SS, HIGH);
  
  Serial.println("ready");
}

void loop() {
  byte msg;

  
  // put your main code here, to run repeatedly:
  if(frontV_en)
  {
    #ifdef DEBUG
      Serial.print("Front vertical: ");
      Serial.println(ping_frontV.ping_cm());
    #endif
    if(ping_frontV.ping_cm() > vertFront_threshold)
    {
      delay(confirmationDelay);
      if(ping_frontV.ping_cm() > vertFront_threshold)
      {
        //Cliff detected
        #ifdef DEBUG
          Serial.println("Front cliff detected");
        #endif
        pinMode(motorEstop_pin, OUTPUT);
        delay(1);
        digitalWrite(motorEstop_pin, LOW);
        //Chantelle put message here
        Serial.println("cliff");
      }
    }
    else
    {
      pinMode(motorEstop_pin, INPUT);
    }
  }

  delay(readingDelay);

  if(backV_en)
  {
    #ifdef DEBUG
      Serial.print("Back vertical: ");
      Serial.println(ping_backV.ping_cm());
    #endif
    if(ping_backV.ping_cm() > vertBack_threshold)
    {
      delay(confirmationDelay);
      if(ping_backV.ping_cm() > vertBack_threshold)
      {
        //Cliff detected
        #ifdef DEBUG
          Serial.println("Back cliff detected");
        #endif
        pinMode(motorEstop_pin, OUTPUT);
        delay(1);
        digitalWrite(motorEstop_pin, LOW);
        //Chantelle put message here
      }
    }
    else
    {
      pinMode(motorEstop_pin, INPUT);
    }
  }

  delay(readingDelay);

  if(frontH_en)
  {
    #ifdef DEBUG
      Serial.print("Front horizontal: ");
      Serial.println(ping_frontH.ping_cm());
    #endif
    if(ping_frontH.ping_cm() < horiz_threshold)
    {
      delay(confirmationDelay);
      if(ping_frontH.ping_cm() < horiz_threshold)
      {
        //Obstacle Detected
        #ifdef DEBUG
          Serial.println("Front obstacle detected");
        #endif
        //Chantelle put message here
   
        digitalWrite(SS, LOW);
        msg = 1;
        SPI.transfer(msg);
        delay(50);
        
      }
    } else {
      digitalWrite(SS, LOW);
      msg = 0;
      SPI.transfer(msg);
      delay(50);
    }
  }

  delay(readingDelay);

  if(backH_en)
  {
    #ifdef DEBUG
      Serial.print("Back Horizontal: ");
      Serial.println(ping_backH.ping_cm());
    #endif
    if(ping_backH.ping_cm() < horiz_threshold)
    {
      delay(confirmationDelay);
      if(ping_backH.ping_cm() < horiz_threshold)
      {
        //Obstacle Detected
        #ifdef DEBUG
          Serial.println("Back obstacle detected");
        #endif
        //Chantelle put message here
        digitalWrite(SS, LOW);
        msg = 1;
        SPI.transfer(msg);
        delay(50);
      }
    } else {
      digitalWrite(SS, LOW);
      msg = 0;
      SPI.transfer(msg);
      delay(50);
    }
  }

  delay(readingDelay);
  
  
  
}
