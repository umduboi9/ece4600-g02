/* @file firmware_main.ino
 * 
 * @author Chantelle G. Dubois (umduboi9@myumanitoba.ca)
 * @date 13 November, 2018
 * 
 * @purpose Main board firmware for the ECE 4600: G02 capstone project.
 *          Firmware for: (1) Bluetooth (RN-42, Bluesmirf)
 *                        (2) Speed Indicator
 *                        (3) Controller Interfaces
 *                        (4) Motor Controller
 *                        (5) Watchdog & Comms Between Main and Peripheral Board
 * 
 */

#include <Arduino.h>
#include <SoftwareSerial.h> 
#include <Math.h>
#include <Adafruit_NeoPixel.h>
#include <TimerOne.h>
#include <string.h>
#include <SPI.h>

/* * * * * * * * * * * * * * *
 * MAIN BOARD INIT
 * * * * * * * * * * * * * * */
#define BAUDRATE 9600                       // software serial [bps]                          

/* * * * * * * * * * * * * * *
 * MOTOR CONTROLLER INIT
 * * * * * * * * * * * * * * */
#define MOTOR_ADDRESS      128              // address of motor
#define MOTOR_PIN          6                // S1 TX on pin 6
#define NUM_MOTORS         2                // number of motors
#define EMERGENCY_STOP_PIN 3                // S2 EStop readout on pin D3, when low then EStop activated
#define CHECKSUM           127              // checksum
#define SPEED_OFFSET       2                // speed quantization offset
#define RAMP_LVL           2                // ramp up/down agression (0 - no ramp, larger number is more agressive)

enum Mode       {STOP, DRIVE};
enum SpeedLvl   {OFF, MIN, LVL1, LVL2, LVL3, LVL4, LVL5, LVL6, LVL7, LVL8, LVL9, LVL10, MAX};  
enum Command    {M1_FWD, M1_REV, MIN_VOL, MAX_VOL, M2_FWD, M2_REV, MAX_M1_FWD, MAX_M2_FWD}; // as defined in manual
enum Controller {INVALID, BLUETOOTH, JOYSTICK};      
enum Motor      {M1, M2};

int currSpeed[NUM_MOTORS], targetSpeed[NUM_MOTORS], motorDirection[NUM_MOTORS];

Mode       currMode;                        // current mode of motor
SpeedLvl   currSpeedLvl;                    // selected max speed level, also target speed for motors
Controller currController;                  // selected controller interface

SoftwareSerial MotorSerial(NOT_A_PIN, MOTOR_PIN);   // TX pin only, no RX pin

void motorControllerSetup();
void sendSerialPacketToMotor(int address, int command, int data);
Mode getMode();
void setMode(Mode mode);
SpeedLvl getSpeedLvl();
void setSpeedLvl(SpeedLvl speed);
int getCurrSpeed(Motor motor);
void setCurrSpeed(Motor motor, float speed);
void setTargetSpeed(Motor motor, int speed);
void calculateMotorParams();
void rampUp(Motor motor);
void rampDown(Motor motor);
void stop();
void emergencyStop(bool stopok);
void drive();    

/* * * * * * * * * * * * * * *
 * SPEED INDICATOR INIT
 * * * * * * * * * * * * * * */
#define NEOPIXEL_PIN 7                      // neopixel pin
#define SPEED_PIN    A3                     // speed trigger pin
#define BRIGHTNESS   1                      // brightness (1 = full brightness)
#define PIXEL_LENGTH 12                     // pixel length
#define DEBOUNCE_T   50                     // debounce delay [ms]
#define HOLD_T       1000                   // hold time [ms] (1000 = 1s)

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(PIXEL_LENGTH, NEOPIXEL_PIN, NEO_GRB + NEO_KHZ800);

int buttonState[2];                         // [0] last state, [1] curr state          
unsigned long lastDebounceTime;             // last clocked time for debounce

struct RGB{
  byte red;
  byte green;
  byte blue;
};

RGB red        = {255, 0, 0};
RGB green      = {0, 255, 0};
RGB lightGreen = {0, 50, 0};
RGB blue       = {0, 0 ,255};
RGB lightBlue  = {0, 0, 50};
RGB orange     = {255, 40, 0};
RGB yellow     = {255, 220, 0};
RGB stby       = {0, 0, 10};
RGB off        = {0, 0, 0};

void speedIndicatorSetup();
void scanSpeedIndicator();
void lightSegments(int speed, RGB colour);
void allOn(RGB colour);

/* * * * * * * * * * * * * * *
 * CONTROLLER INTERFACE INIT
 * * * * * * * * * * * * * * */
enum XY {X, Y};

// Joystick Parameters
#define JOYSTICK_PIN_X     A0              // analog pin 
#define JOYSTICK_PIN_Y     A1              // analog pin 
#define JOYSTICK_TOLERANCE 10              // tolerance for deadbands/neutral columns [%]

int joystickMin [2]         = {405, 411};
int joystickMax [2]         = {608, 597};
int joystickNeutral [2]     = {509, 510};
int joystickRange [2]       = {joystickMax[X] - joystickMin[X], joystickMax[Y] - joystickMin[Y]};
int joystickMinCentered [2] = {joystickNeutral[X] - joystickRange[X]/2, joystickNeutral[Y] - joystickRange[Y]/2};
int joystickMaxCentered [2] = {joystickNeutral[X] + joystickRange[X]/2, joystickNeutral[Y] + joystickRange[Y]/2};
int joystickRead [2]        = {0, 0};

// Bluetooth parameters
#define BTCTR_TIMEOUT 1000000               // bluetooth control timeout [us]

enum BTDir {BTCTRERR, NTR, FWD, BWD, LFT, RGT};
int currBTDir;

void controllerSetup();
void setCurrController(Controller controller);
Controller getCurrController();
void BTControlTimeout();

/* * * * * * * * * * * * * * *
 * BLUETOOTH INIT
 * * * * * * * * * * * * * * */
#define BT_RX         4                   // digital pin 5
#define BT_TX         5                   // digital pin 4
#define BT_TERMINATOR '/'                 // terminating character for bluetooth msg
#define BT_BEGIN      '@'                 // beginning character for bluetooth msg
#define BUFFER        32                  // buffer size

bool BTStop;                      // flag for BT stop

SoftwareSerial BTSerial(BT_TX, BT_RX);    // software serial for bluetooth

void bluetoothSetup();
void readBluetooth();

/* * * * * * * * * * * * * * *
 * SYSTEM STATUS INIT
 * * * * * * * * * * * * * * */
#define SENSOR_PIN 2                      // low if cliff detected

enum Sensor {NOMINAL, OBJ1, OBJ2, OBJ3, EDGE1, EDGE2, EDGE3};     

volatile byte obj;
SpeedLvl speeds [2];

void systemStatusSetup();
void scanSystemStatus();

/* * * * * * * * * * * * * * *
 * MAIN BOARD SETUP
 * * * * * * * * * * * * * * */
void setup() {
  Serial.begin(BAUDRATE);                  // begin serial monitor
  Serial.println("Hello world!");

  motorControllerSetup();                 // setup motor controller

  speedIndicatorSetup();                  // setup speed indicator

  controllerSetup();                      // setup controller interface

  bluetoothSetup();                       // setup bluetooth

  //systemStatusSetup();                    // setup system status watchdog

} // end setup

/* * * * * * * * * * * * * * *
 * MAIN BOARD LOOP
 * * * * * * * * * * * * * * */
void loop() {

  emergencyStop((digitalRead(EMERGENCY_STOP_PIN) && !BTStop));  // check emergency stop pin  
  
  //scanSystemStatus();   // scan system status

  scanSpeedIndicator();                   // scan speed indicator and update speed level

  readBluetooth();                        // check for bluetooth commands
  
  drive();                                // drive based on parameters 

} // end loop

/* * * * * * * * * * * * * * *
 * MOTOR CONTROL FUNCTIONS
 * * * * * * * * * * * * * * */

/* motorControllerSetup: iniitates parameters for motor controller
* @input {None}
* @return {void}
*/
void motorControllerSetup() {
  pinMode(MOTOR_PIN, OUTPUT);              // init motor controller pin
  pinMode(EMERGENCY_STOP_PIN, INPUT);      // init emergency stop pin

  MotorSerial.begin(BAUDRATE);             // software serial begin 

  currMode       = DRIVE;                  // initial motor mode
  currSpeedLvl   = LVL5;                   // initial max speed of motor
  currController = JOYSTICK;               // initial controller

  currSpeed[M1]      = OFF;                // initial speed motor 1
  currSpeed[M2]      = OFF;                // initial speed motor 2
  targetSpeed[M1]    = OFF;                // initial target speed motor 1
  targetSpeed[M2]    = OFF;                // initial target speed motor 2
  motorDirection[M1] = M1_FWD;             // initial direction motor 1
  motorDirection[M2] = M2_FWD;             // initial direction motor 2
}

/* SendSerialPacketToMotor: Sends packet to motor controller
*
*  Protocol for sending packets to sabertooth is to send address, command, data,
*  and then sending the 3 combined and masked with the checksum.
*
* @input {int}address, {int}command, {int}data
* @return {void}
*/
void sendSerialPacketToMotor(int address, int command, int data) {
  MotorSerial.write(address);
  MotorSerial.write(command);
  MotorSerial.write(data);
  MotorSerial.write((address + command + data) & CHECKSUM);
}

/* getMode: returns current mode
* @input {None}
* @return {Mode}currMode
*/
Mode getMode() {
  return currMode;
}

/* setMode: sets current mode
* @input {Mode}mode
* @return {void}
*/
void setMode(Mode mode) {
  currMode = mode;
}

/* getSpeedLvl: returns current max speed
* @input {None}
* @return {SpeedLvl}maxSpeed
*/
SpeedLvl getSpeedLvl() {
  return currSpeedLvl;
}

/* setSpeedLvl: sets max speed
* @input {SpeedLvl}speed
* @return {void}
*/
void setSpeedLvl(SpeedLvl lvl) {
  currSpeedLvl = lvl;
}

/* getCurrSpeed: returns speed of selected motor
* @input {Motor}motor
* @return {int}currSpeed
*/
int getCurrSpeed(Motor motor) {
  return currSpeed[motor];
}

/* getMaxSpeed: returns absolute max speed
* @inputs {None}
* @return {int}maxSpeed
*/
int getMaxSpeed() {
  return currSpeedLvl*SPEED_OFFSET;
}

/* setCurrSpeed: sets speed of selected motor, cannot surpass max
* @input {Motor}motor, {int}speed
* @return {void}
*/
void setCurrSpeed(Motor motor, int speed) {
    currSpeed[motor] = speed;
}

/* setTargetSpeed: sets target speed of selected motor, cannot surpass max
* @input {Motor}motor, {int}speed
* @return {void}
*/
void setTargetSpeed(Motor motor, int speed) {
    targetSpeed[motor] = speed;
}

/* stop: sets mode to STOP, speed to OFF, stops motors - comes to sudden stop
* @input {None}
* @return {None}
*/
void stop() {
  setMode(STOP);
  setCurrSpeed(M1, OFF);
  setCurrSpeed(M2, OFF);
  sendSerialPacketToMotor(MOTOR_ADDRESS, motorDirection[M1], currSpeed[M1]);
  sendSerialPacketToMotor(MOTOR_ADDRESS, motorDirection[M2], currSpeed[M2]);
}

/* emergencyStop: sets mode to STOP, speed to OFF, stops motors - sudden stop
* @input {None}
* @return {void}
*/
void emergencyStop(bool stopok) {
  // sysok should be estop pin, will be low if emergency stop is flagged
  if (!stopok && getMode() == DRIVE) {

      // our beautiful hacky solution is to set this high, brake, then go back to listening
      pinMode(EMERGENCY_STOP_PIN, OUTPUT);
      delay(5);

      digitalWrite(EMERGENCY_STOP_PIN, HIGH);
      delay(5);

      // needs to loop through stop to disperse momentum
      int count = 0;
      while (count < 50) {
        stop(); // put in stop mode and apply brakes
        count++;
      }

      digitalWrite(EMERGENCY_STOP_PIN, LOW);
      delay(5);

      pinMode(EMERGENCY_STOP_PIN, INPUT); // return to listening while in stop mode
    }
 
  // recover from emergency stop
  else if (stopok && getMode() == STOP) {
    setMode(DRIVE);
  }
}

/* setDirection: sets direction of motor
* @input {Motor}motor, {Command}direction
* @return {void}
*/
void setDirection(Motor motor, Command direction) {
  motorDirection[motor] = direction;
}

/* calculateMotorParams: calculates speed and direction of wheels 
* @input {None}
* @return {void}
*/
void calculateMotorParams() {
  int speedMappedX, speedMappedY, delta, delta_adj[2];
  
  // controller interface parameter updates is dependent on controller
  switch(getCurrController()) {

    case JOYSTICK:

      // reads analog joystick pins and converts voltage to %
      joystickRead[X]= map(analogRead(JOYSTICK_PIN_X), joystickMinCentered[X], joystickMaxCentered[X], -100, 100);
      joystickRead[Y]= map(analogRead(JOYSTICK_PIN_Y), joystickMinCentered[Y], joystickMaxCentered[Y], -100, 100);

      // bound the readings between -100 and 100
      if (abs(joystickRead[X]) > 100) {
        joystickRead[X] = 100 * (joystickRead[X]/abs(joystickRead[X]));
      }

      if (abs(joystickRead[Y]) > 100) {
        joystickRead[Y] = 100 * (joystickRead[Y]/abs(joystickRead[Y]));
      }

      // map to current speed level
      speedMappedX = map(joystickRead[X], -100, 100, -getMaxSpeed(), getMaxSpeed());
      speedMappedY = map(joystickRead[Y], -100, 100, -getMaxSpeed(), getMaxSpeed());
      
      // joystick is neutral
      if (abs(joystickRead[X]) < JOYSTICK_TOLERANCE && abs(joystickRead[Y]) < JOYSTICK_TOLERANCE) {
        setTargetSpeed(M1, OFF);
        setTargetSpeed(M2, OFF);
        break;
        
       // forward
       } else if (abs(joystickRead[X]) < JOYSTICK_TOLERANCE && joystickRead[Y] > JOYSTICK_TOLERANCE) {
          setDirection(M1, M1_FWD);
          setDirection(M2, M2_FWD);
          setTargetSpeed(M1, abs(speedMappedY*SPEED_OFFSET));
          setTargetSpeed(M2, abs(speedMappedY*SPEED_OFFSET));
          break;

        // backward
       } else if (abs(joystickRead[X]) < JOYSTICK_TOLERANCE && joystickRead[Y] < -JOYSTICK_TOLERANCE) {
          setDirection(M1, M1_REV);
          setDirection(M2, M2_REV);
          setTargetSpeed(M1, abs(speedMappedY*SPEED_OFFSET));
          setTargetSpeed(M2, abs(speedMappedY*SPEED_OFFSET));
          break;

      // left
      } else if (joystickRead[X] < -JOYSTICK_TOLERANCE && abs(joystickRead[Y]) < JOYSTICK_TOLERANCE) {
          setDirection(M1, M1_FWD);
          setDirection(M2, M2_REV);
          setTargetSpeed(M1, abs(speedMappedX*SPEED_OFFSET));
          setTargetSpeed(M2, abs(speedMappedX*SPEED_OFFSET));
          break;

      // right
      } else if (joystickRead[X] > JOYSTICK_TOLERANCE && abs(joystickRead[Y]) < JOYSTICK_TOLERANCE) {
          setDirection(M1, M1_REV);
          setDirection(M2, M2_FWD);
          setTargetSpeed(M1, abs(speedMappedX*SPEED_OFFSET));
          setTargetSpeed(M2, abs(speedMappedX*SPEED_OFFSET));
          break;

      } else {
        
        // radius turning (mixing algorithm courtesy of Shea)
        int magnitude = round(sqrt((sq(speedMappedY*SPEED_OFFSET)+sq(speedMappedX*SPEED_OFFSET))));
        float theta = map(round(atan2(abs(speedMappedY*SPEED_OFFSET), abs(speedMappedX*SPEED_OFFSET))*(180/3.1415926)), 0, 130, 0, 100);
        float direction1 = theta/100;

        int delta_adj [NUM_MOTORS] = {magnitude, magnitude*direction1};
        
        // quadrant 1 (top right)
        if (joystickRead[X] > JOYSTICK_TOLERANCE && joystickRead[Y] > JOYSTICK_TOLERANCE) {
          setDirection(M1, M1_FWD);
          setDirection(M2, M2_FWD);
          setTargetSpeed(M2, delta_adj[0]); // outer wheel
          setTargetSpeed(M1, delta_adj[1]); // inner wheel
          break;
        }

        // quadrant 2 (bottom right)
        else if (joystickRead[X] > JOYSTICK_TOLERANCE && joystickRead[Y] < -JOYSTICK_TOLERANCE) {
          setDirection(M1, M1_REV);
          setDirection(M2, M2_REV);
          setTargetSpeed(M1, delta_adj[1]); // inner wheel
          setTargetSpeed(M2, delta_adj[0]); // outer wheel
          break;
        }

        // quadrant 3 (bottom left)
        else if (joystickRead[X] < -JOYSTICK_TOLERANCE && joystickRead[Y] < -JOYSTICK_TOLERANCE) {
          setDirection(M1, M1_REV);
          setDirection(M2, M2_REV);
          setTargetSpeed(M1, delta_adj[0]); // outer wheel
          setTargetSpeed(M2, delta_adj[1]); // inner wheel
          break;
        }

        // quadrant 4 (top left)
        else if (joystickRead[X] < -JOYSTICK_TOLERANCE && joystickRead[Y] > JOYSTICK_TOLERANCE) {
          setDirection(M1, M1_FWD);
          setDirection(M2, M2_FWD);
          setTargetSpeed(M2, delta_adj[1]); // inner wheel
          setTargetSpeed(M1, delta_adj[0]); // outer wheel
          break;
        }
     }
     break; // end JOYSTICK

    case BLUETOOTH:

        int speed = currSpeedLvl*SPEED_OFFSET;

        // neutral
        if (currBTDir == NTR) {
          Serial.println("n");
          setTargetSpeed(M1, OFF);
          setTargetSpeed(M2, OFF);
        }

        // point turn left
        else if (currBTDir == LFT) {
          Serial.println("l");
          setDirection(M1, M1_FWD);
          setDirection(M2, M2_REV);
          setTargetSpeed(M1, speed);
          setTargetSpeed(M2, speed);
        }

        // point turn right
        else if (currBTDir == RGT) {
          Serial.println("r");
          setDirection(M1, M1_REV);
          setDirection(M2, M2_FWD);
          setTargetSpeed(M1, speed);
          setTargetSpeed(M2, speed);
        }

        // forward
        else if (currBTDir == FWD) {
          Serial.println("f");
          setDirection(M1, M1_FWD);
          setDirection(M2, M2_FWD);
          setTargetSpeed(M1, speed);
          setTargetSpeed(M2, speed);
        }

        // backward
        else if (currBTDir == BWD) {
          Serial.println("b");
          setDirection(M1, M1_REV);
          setDirection(M2, M2_REV);
          setTargetSpeed(M1, speed);
          setTargetSpeed(M2, speed);
        }
      
      break; // end BLUETOOTH
     
  } // end switch statement 

  // ramp wheels up or down for M1
  if (getCurrSpeed(M1) > targetSpeed[M1]) {
    rampDown(M1);
  } else if (getCurrSpeed(M1) < targetSpeed[M1]) {
    rampUp(M1);
  } else {
    setCurrSpeed(M1, targetSpeed[M1]);
  }

  // ramp wheels up or down for M2
  if (getCurrSpeed(M2) > targetSpeed[M2]) {
    rampDown(M2);
  } else if (getCurrSpeed(M2) < targetSpeed[M2]) {
    rampUp(M2);
  } else {
    setCurrSpeed(M2, targetSpeed[M2]);
  }

}

/* rampUp: increments selected motor speed
* @inpuut {Motor}motor
* @return {void}
*/
void rampUp(Motor motor) {
  setCurrSpeed(motor, getCurrSpeed(motor) + RAMP_LVL);
}

/* rampDown: decrements selected motor speed
* @inpuut {Motor}motor
* @return {void}
*/
void rampDown(Motor motor) {
  setCurrSpeed(motor, getCurrSpeed(motor) - RAMP_LVL);
}

/* drive: updates motor parameters and then sends data to motors
* @input {None}
* @return {void}
*/
void drive() {
  if (getMode() == DRIVE) {
    // first calculate new speeds and direction of wheels, then send vals to motors
    calculateMotorParams();
    sendSerialPacketToMotor(MOTOR_ADDRESS, motorDirection[M1], currSpeed[M1]);
    sendSerialPacketToMotor(MOTOR_ADDRESS, motorDirection[M2], currSpeed[M2]);
  } else {
    // in stop mode, so send 0 speed to motors
    sendSerialPacketToMotor(MOTOR_ADDRESS, motorDirection[M1], OFF);
    sendSerialPacketToMotor(MOTOR_ADDRESS, motorDirection[M2], OFF);
  }

  delay(20);
}

/* * * * * * * * * * * * * * *
 * SPEED INDICATOR FUNCTIONS
 * * * * * * * * * * * * * * */

/* speedIndicatorSetup: set up parameters for speed indicator
* @input {None}
* @return {void}
*/
void speedIndicatorSetup() {
  pinMode(SPEED_PIN, INPUT_PULLUP);         // pullup is super important
  buttonState[0] = HIGH;                    // init last button state
  pixels.begin();
  lightSegments(currSpeedLvl, orange);      // initialize lights to init speed
}

/* scanSpeedIndicator: scans the speed indicator hardware for speed changes 
* @input {None}
* @output {void}
*/
void scanSpeedIndicator() {
  int speedPinRead = digitalRead(SPEED_PIN);  // read current state of speed trigger

  if (speedPinRead != buttonState[0]) {
    // if the button state has toggled from last known state
    lastDebounceTime = millis();
  }
  
  if((millis() - lastDebounceTime) > DEBOUNCE_T) {
    // if the read value from pin is not current known state
    if (speedPinRead != buttonState[1]) {
      buttonState[1] = speedPinRead;
      if (buttonState[1] == LOW) {
        if (getSpeedLvl() == MAX) {
          // if we are at max speed, and we trigger speed change again, it loops around to min
          setSpeedLvl(MIN);
        } else {
          // increment speed level
          setSpeedLvl((int)getSpeedLvl() + 1);
        }
      }
    } else if (millis() - lastDebounceTime > HOLD_T) { // pin being held for reset?
      if (speedPinRead == buttonState[1] && buttonState[1] == LOW) {
        setSpeedLvl(MIN);
      }
    }
  }
  // update lighted segments and update last known pin state
  allOn(off);
  lightSegments(getSpeedLvl(), orange);
  buttonState[0] = speedPinRead;
}

/* lightSegments: lights the segments corresponding to current speed
* @input {int}speed {RGB}colour
* @output {void}
*/
void lightSegments(int speed, RGB colour) {
  for(int i=1; i<=speed; i++) {
    pixels.setPixelColor(i, pixels.Color(colour.red/BRIGHTNESS, colour.green/BRIGHTNESS, colour.blue/BRIGHTNESS));
  }
  pixels.show();
}

/* allOn: turns all light segments on to colour selected (useful for turning it off)
* @input {RGB}colour
* @output {void}
*/
void allOn(RGB colour) {
  for(int i=1; i<=PIXEL_LENGTH; i++) {
    pixels.setPixelColor(i, pixels.Color(colour.red/BRIGHTNESS, colour.green/BRIGHTNESS, colour.blue/BRIGHTNESS));
  }
  pixels.show();
}

/* * * * * * * * * * * * * * * * * 
 * CONTROLLER INTERFACE FUNCTIONS
 * * * * * * * * * * * * * * * * */

/* controllerSetup: set up for interface controller
* @input {None}
* @return {void}
*/
void controllerSetup() {

  switch(getCurrController()) {
  // only required for physical controller interfaces (ie, not bluetooth)

    case JOYSTICK:
      pinMode(JOYSTICK_PIN_X, INPUT);       // no pullups on joystick pins
      pinMode(JOYSTICK_PIN_Y, INPUT);
      break;
  }
}

/* setCurrController: sets the current controller being used
* @input {Controller}controller
* @return {void}
*/
void setCurrController(Controller controller) {
    currController = controller;
}

/* setCurrController: gets the current controller being used
* @input {None}
* @return {Controller}currController
*/
Controller getCurrController() {
  return currController;
}

/* * * * * * * * * * * * * * * * * 
 * BLUETOOTH FUNCTIONS
 * * * * * * * * * * * * * * * * */

/* bluetoothSetup: setup for bluetooth module 
* @input {None} 
* @return {void}
*/
void bluetoothSetup() {
  BTSerial.begin(115200);                   // default on bootup
  BTStop = false;

  // need to change baudrate everytime there is a power cycle, 115200 is too fast for SW serial
  BTSerial.print("$");               
  BTSerial.print("$");
  BTSerial.print("$");                      
  delay(100);                                
  BTSerial.println("U,9600,N");             // BT modem baudrate changed to 9600

  BTSerial.begin(BAUDRATE);                 // BT software serial begins at 9600  
  delay(200);


}

/* readBluetooth: checks if any bluetooth data is being sent over serial and processes as necessary
* @input {None}
* @return {void}
*/
void readBluetooth() {
   BTSerial.listen();  // listen to BT port
   delay(50);
  
  if(BTSerial.available())  // If the bluetooth sent any characters
  {    
    char data = (char)BTSerial.read();

    // save data to buffer
    if (data == BT_BEGIN) {
      
      int count = 0;
      char BTBuffer[BUFFER];
      
      while(data != BT_TERMINATOR && count < BUFFER) {
        data = (char)BTSerial.read();
        BTBuffer[count++] = data;
      }

      BTBuffer[count-1] = '\0';
      delay(10);

      // convert to String and process
      char *BTData = BTBuffer;
      String BTCommand;

      BTCommand = strtok(BTData, ",");

      while (BTCommand != NULL) {

          if (BTCommand.equals("BTSTP"))
          {
            BTStop = true;
    
            BTSerial.write("@AOK");
            BTSerial.flush();
            delay(10);
          }

          else if (BTCommand.equals("BTGO"))
          {
            BTStop = false;
          
            BTSerial.write("@AOK");
            BTSerial.flush();
            delay(10);
          }

          else if (BTCommand.equals("BTSPD"))
          {
            BTCommand = strtok(NULL, ",");

            if (BTCommand.equals("0")) {
              Serial.println("reducing speed");

              if (currSpeedLvl == MIN) {
                setSpeedLvl(MIN);
              } else {
                setSpeedLvl(getSpeedLvl() - 1);
              }
              
            } else if (BTCommand.equals("1")) {
              
              if (currSpeedLvl == MAX) {
                setSpeedLvl(MIN);
              } else {
                setSpeedLvl(getSpeedLvl() + 1);
              }
            }
            
            BTSerial.write("@AOK");
            BTSerial.flush();
            BTSerial.write(currSpeedLvl);
            BTSerial.flush();
            delay(10);
          }

          else if (BTCommand.equals("BTCTR")) {

            if (getCurrController() != BLUETOOTH) {
              setCurrController(BLUETOOTH);
            }

            BTCommand = strtok(NULL, ",");
            
            currBTDir = (BTDir)BTCommand.toInt();
            
            // let timer expire if in neutral
            if (currBTDir != NTR) {
              Timer1.initialize(BTCTR_TIMEOUT);         // begin timeout for bluetooth control
              Timer1.attachInterrupt(BTControlTimeout); // attach interrupt callback
            }

            break;
          }

          else {  
            Serial.println("ERROR - unknown command or data corrupted");
            BTSerial.write("@ERR");
            BTSerial.flush();
          }
       
          BTCommand = strtok(NULL, ",");
      }
    }
  }
}

/* BTControlTimeout: the callback function for when the bluetooth controller timesout
* @input {None}
* @return {void}
*/
void BTControlTimeout() {
  if (getCurrController() == BLUETOOTH) {
    setCurrController(JOYSTICK);
    BTSerial.write("@JOY");
    BTSerial.flush();
  }
}

/* * * * * * * * * * * * * * * * * 
 * SYSTEM STATUS FUNCTIONS
 * * * * * * * * * * * * * * * * */

/* systemStatusSetup: sets up parameters for the system status indicated by mp2
* @input {None}
* @return {void}
*/
void systemStatusSetup() {
  pinMode(MISO, OUTPUT);
  SPCR |= _BV(SPE); // turn on SPI in slave mode
  SPI.attachInterrupt(); // turn on interrupt

  speeds[0] = getSpeedLvl();
  speeds[1] = LVL3;
}

/* scanSystemStatus: scans to see if there is a system status alert
* @input {bool}sysok
* @return {void}
*/
void scanSystemStatus() {

  // object detected
  if (obj && getSpeedLvl() != speeds[1]) {
    speeds[0] = getSpeedLvl(); 
    setSpeedLvl(speeds[1]);

  // nominal
   } else if (!obj && getSpeedLvl() != speeds[0]) {
    setSpeedLvl(speeds[0]);
   }
}

ISR (SPI_STC_vect) // SPI interrupt routine 
{ 
  obj = SPDR;          
}