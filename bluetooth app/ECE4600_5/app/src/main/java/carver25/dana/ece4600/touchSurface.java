package carver25.dana.ece4600;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorSpace;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.widget.Toast;

import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

public class touchSurface extends SurfaceView implements SurfaceHolder.Callback {

    public Canvas canvas;
    Bitmap joystick;
    Bitmap backBuffer;
    int width, height, clientHeight;
    Paint paint;
    Paint paint1;
    Paint paint2;
    Paint paint3;
    Paint paint6;
    Paint paint7;
    Context context;
    SurfaceHolder mHolder;
    Thread thread;
    public static int X, Y;
    public float cx, cy, dx, dy, jx, jy, theta, b_radius, j_radius, buffer;
    public float y1, y2, y3, y4, x1, x2, x3, x4, slope1, slope2, int1, int2;
    public String direction, pos;

    public touchSurface(Context context) {
        super(context);
        this.context = context;
        init();
    }
    public touchSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        mHolder = getHolder();
        mHolder.addCallback(this);
        paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(10);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint1 = new Paint();
        paint1.setColor(Color.rgb(255, 110, 0));
        paint2 = new Paint();
        paint2.setTextSize(70);
        paint2.setStrokeWidth(8);
        paint2.setColor(Color.rgb(0, 0, 0));
        paint3 = new Paint();
        paint3.setColor(Color.rgb(255, 255, 255));
        paint3.setTextSize(70);
        paint3.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        paint6 = new Paint();
        paint6.setColor(Color.rgb(255, 255, 255));
        paint6.setTextSize(45);
        paint7 = new Paint();
        paint7.setColor(Color.rgb(255, 255, 0));
        paint7.setTextSize(45);
        joystick = BitmapFactory.decodeResource(getResources(),R.drawable.black_sphere);
        j_radius = joystick.getWidth()/2;
        buffer = j_radius/3;
    }

    int lastX, lastY;
    double dist;
    boolean isDeleting;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        int action = event.getAction();
        X = (int) event.getX();
        Y = (int) event.getY();
        dx = X - cx;
        dy = Y - cy;
        theta = (float)Math.atan(Math.abs(dy/dx));
        switch(action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                lastX = (int) event.getX();
                lastY = (int) event.getY();
                jx = cx-joystick.getWidth()/2;
                jy = cy-joystick.getHeight()/2;
                canvas.drawBitmap(joystick, jx, jy, null);
                break;
            case MotionEvent.ACTION_MOVE:
                if(isDeleting) break;
                dx = event.getX()-cx;
                dy = event.getY()-cy;
                dist = Math.sqrt(Math.pow(cx - event.getX(),2) + Math.pow(cy - event.getY(),2));
                if(dist<j_radius*1.5) {
                    jx = X-joystick.getWidth()/2;
                    jy = Y-joystick.getHeight()/2;
                    canvas.drawBitmap(joystick, jx, jy, null);
                }
                else {
                    if(dx > 0 && dy > 0) {
                        jx = (float)(cx + b_radius*Math.cos(theta) - joystick.getWidth()/2);
//                        X = (int)cx + (int) Math.round(b_radius*Math.cos(theta));
                        jy = (float)(cy + b_radius*Math.sin(theta) - joystick.getHeight()/2);
//                        Y = (int)cy + (int) Math.round(b_radius*Math.sin(theta));
                    }
                    else if(dx < 0 && dy > 0) {
                        jx = (float)(cx - b_radius*Math.cos(theta) - joystick.getWidth()/2);
//                        X = (int)cx + (int) Math.round(b_radius*Math.cos(theta));
                        jy = (float)(cy + b_radius*Math.sin(theta) - joystick.getHeight()/2);
//                        Y = (int)cy + (int) Math.round(b_radius*Math.cos(theta));
                    }
                    if(dx > 0 && dy < 0) {
                        jx = (float)(cx + b_radius*Math.cos(theta) - joystick.getWidth()/2);
                        jy = (float)(cy - b_radius*Math.sin(theta) - joystick.getHeight()/2);
                    }
                    if(dx < 0 && dy < 0) {
                        jx = (float)(cx - b_radius*Math.cos(theta) - joystick.getWidth()/2);
                        jy = (float)(cy - b_radius*Math.sin(theta) - joystick.getHeight()/2);
                    }
                }
                if(dist < buffer)
                    direction = "1"; // N
                if(dist > buffer) {

                    if (X > (Y-int1)/slope1 && X < (Y-int2)/slope2) {
                        direction = "2"; //F
                        //canvas.drawText(direction, cx, 300, paint3);
                    }
                    if (X > (Y-int1)/slope1 && X > (Y-int2)/slope2) {
                        direction = "5"; //R
                        //canvas.drawText(direction, 1000, 600, paint3);
                    }
                    if (X < (Y-int1)/slope1 && X < (Y-int2)/slope2) {
                        direction = "4"; //L
                        //canvas.drawText(direction, 50, 600, paint3);
                    }
                    if (X < (Y-int1)/slope1 && X > (Y-int2)/slope2) {
                        direction = "3"; //B
                        //canvas.drawText(direction, 200, 1400, paint3);
                    }
                }
                canvas.drawBitmap(joystick, jx, jy, null);
//                lastX = X;
//                lastY = Y;
//                Toast.makeText(context, Float.toString(Math.abs(cx)),Toast.LENGTH_SHORT).show();
//                pos = "@BTCTR," + Float.toString(jx-cx) + "," + Float.toString(jy-cy) + "," + (b_radius) + "/";
                Toast.makeText(context, direction, Toast.LENGTH_SHORT).show();
                pos = "@BTCTR," + direction + "/";
                SettingsFragment.mBluetooth.write(pos.getBytes(Charset.defaultCharset()));
                try {
                    TimeUnit.MILLISECONDS.sleep(30);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }

//                for(int i = 0; i < 1000; i++){}
//                    SettingsFragment.mBluetooth.write(Integer.toString(i).getBytes(Charset.defaultCharset()));

                break;

            case MotionEvent.ACTION_UP:
                if(isDeleting) isDeleting = false;
                canvas.drawBitmap(joystick,canvas.getWidth()/2-joystick.getWidth()/2, canvas.getHeight()/2-joystick.getHeight()/2, null);
                direction = "1"; //N
                pos = "@BTCTR," + direction + "/";
                SettingsFragment.mBluetooth.write(pos.getBytes(Charset.defaultCharset()));
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                isDeleting = true;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
        }
        draw();
        return true;
    }

    protected void draw() {
        if(clientHeight==0) {
            clientHeight = getClientHeight();
            height = clientHeight;
            canvas.setBitmap(backBuffer);
            b_radius = canvas.getWidth()/5;
            cx = canvas.getWidth()/2;
            cy = canvas.getHeight()/2;
            y1 = (float)(cy+(float)(width)*Math.cos(45));
            y2 = (float)(cy-canvas.getWidth()*Math.cos(45));
            x1 = (float)(cx+canvas.getWidth()*Math.cos(45));
            x2 = (float)(cx-canvas.getWidth()*Math.cos(45));
            y3 = (float)(cy+canvas.getWidth()*Math.cos(45));
            y4 = (float)(cy-canvas.getWidth()*Math.cos(45));
            x3 = (float)(cx-canvas.getWidth()*Math.cos(45));
            x4 = (float)(cx+canvas.getWidth()*Math.cos(45));
            slope1 = (y1 - y2)/(x1 - x2);
            slope2 = (y3 - y4)/(x3 - x4);
            int1 = (float)(cy+canvas.getWidth()*Math.cos(45)) - slope1*(float)(cx+canvas.getWidth()*Math.cos(45));
            int2 = (float)(cy+canvas.getWidth()*Math.cos(45)) - slope2*(float)(cx-canvas.getWidth()*Math.cos(45));
            canvas.drawColor(Color.rgb(191,188,187));
            canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()/2, b_radius+5, paint2);
            canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()/2, b_radius, paint1);
            canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()*69/80, canvas.getWidth()*35/200, paint2);
            canvas.drawBitmap(joystick, canvas.getWidth()/2-joystick.getWidth()/2, canvas.getHeight()/2-joystick.getHeight()/2, null);
            canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()*69/80, canvas.getWidth()*17/100, paint7);

            //canvas.drawRect(canvas.getWidth()/60*4, canvas.getWidth()/60*4, canvas.getWidth()/60*21, canvas.getWidth()/60*12, paint2);
            //canvas.drawRect(canvas.getWidth()/60*21, canvas.getWidth()/60*5, canvas.getWidth()/60*22, canvas.getWidth()/60*11, paint2);
//            canvas.drawCircle(canvas.getWidth()/100*88, canvas.getWidth()/100*20, 125, paint2);
//            canvas.drawText("SPEED", canvas.getWidth()/100*88-70, canvas.getWidth()/100*20+80, paint6);
            //canvas.drawLine(x1, y1, x2, y2, paint3);
            //canvas.drawLine(x3, y3, x4, y4, paint3);
        }
        Canvas canvas_null = null;
        try{
            canvas_null = mHolder.lockCanvas(null);
            canvas_null.drawBitmap(backBuffer, 0,0, paint);
            canvas.drawColor(Color.rgb(191,188,187));
            canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()/2, b_radius+5, paint2);
            canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()/2, b_radius, paint1);
            canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()*69/80, canvas.getWidth()*35/200, paint2);
            canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()*69/80, canvas.getWidth()*17/100, paint7);
            //canvas.drawRect(canvas.getWidth()/60*4, canvas.getWidth()/60*4, canvas.getWidth()/60*21, canvas.getWidth()/60*12, paint2);
            //canvas.drawRect(canvas.getWidth()/60*21, canvas.getWidth()/60*5, canvas.getWidth()/60*22, canvas.getWidth()/60*11, paint2);
//            canvas.drawCircle(canvas.getWidth()/100*88, canvas.getWidth()/100*20, 125, paint2);
//            canvas.drawText("SPEED", canvas.getWidth()/100*88-70, canvas.getWidth()/100*20+80, paint6);
            //canvas.drawLine(x1, y1, x2, y2, paint3);
            //canvas.drawLine(x3, y3, x4, y4, paint3);
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            if(mHolder!=null)  mHolder.unlockCanvasAndPost(canvas_null);
        }
    }

    private int getClientHeight() {
        Rect rect= new Rect();
        Window window = ((Activity)context).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rect);
        int statusBarHeight= rect.top;
        int contentViewTop= window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight= contentViewTop - statusBarHeight;
        return ((Activity)context).getWindowManager().getDefaultDisplay().
                getHeight() - statusBarHeight - titleBarHeight;
    }
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {

        width = getWidth();
        height = getHeight();
        canvas = new Canvas();
        backBuffer = Bitmap.createBitmap( width, height, Bitmap.Config.ARGB_8888);
        paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(10);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
        init();
        draw();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        thread.isAlive();
        while (retry) {
            try {
                thread.join();
                retry = false;
            }
            catch (InterruptedException e) {
            }
        }
    }
}