package carver25.dana.ece4600;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.charset.Charset;

public class HomeScreen extends Fragment implements Runnable {

    Paint paint10;
    float x, y, dx, dy;
    touchSurface tSurface;
    FrameLayout fLayout;
    TextView speedIndicator;
    public char direction;
    Context mContext;
    SettingsFragment settings;
    Button btnSettings;
    BluetoothAdapter mBluetoothAdapter;
    Bluetooth mBluetooth;
    Runnable myRunnable;
    Handler handler;

    public HomeScreen(Context context){
        mContext = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_main, container, false);
        tSurface = new touchSurface(getContext());
        fLayout = v.findViewById(R.id.frameLayout);
        fLayout.addView(tSurface);
        btnSettings = v.findViewById(R.id.next);
        speedIndicator = v.findViewById(R.id.speedID);
        speedIndicator.setText(Integer.toString(SettingsFragment.speed));
        handler=new Handler();

        handler.postDelayed(myRunnable, 100);
//        btnSettings.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mListener.changeFragment(1);
//            }
//        });

        return v;
    }

//    public void nextActivity(View v){
//        //Intent i = new Intent(this, SettingsActivity.class);
//        //startActivity(i);
//    }
//
//    public void stop(View v){
//        Toast.makeText(mContext,"STOPPING", Toast.LENGTH_LONG).show();
//        byte bytes[] = "@STOP#".getBytes(Charset.defaultCharset());
//    }


    public void resume() {
        super.onResume();

        run();
    }
    @Override
    public void run() {
//        while(true) {
//            if (!speedIndicator.getText().equals(Integer.toString(SettingsFragment.speed))) {
                speedIndicator.setText(Integer.toString(SettingsFragment.speed));
//            }
//             else {
//                Toast.makeText(mContext, "Not working", Toast.LENGTH_SHORT).show();
//            }
//        }
                handler.postDelayed(this, 5000);
    }
}
