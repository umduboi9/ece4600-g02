package carver25.dana.ece4600;


import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.UUID;


public class MainActivity extends AppCompatActivity implements View.OnTouchListener, Runnable, GoFragment.OnFragmentInteractionListener {

    int x, y, prevX, prevY;
    float cx, cy, radius;
    float posX;
    float posY;
    touchSurface tSurface;
    FrameLayout fLayout, frame1;
    public char direction;
    HomeScreen home;
    SettingsFragment settings;
    GoFragment go;
    private static final int PERIOD=5000;
    Runnable myRunnable;
    Handler handler;
    String message;

    public void nextActivity(View v){
        //Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
        //getSupportFragmentManager().beginTransaction().remove(home).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, settings).commit();
        //Intent i = new Intent(this, SettingsActivity.class);
        //startActivity(i);
    }

    public void stop(View v){
//        Toast.makeText(this,"STOPPING", Toast.LENGTH_LONG).show();
        byte bytes[] = "@BTSTP/".getBytes(Charset.defaultCharset());
//        byte bytes[] = "@BTSPD,1/".getBytes(Charset.defaultCharset());
            SettingsFragment.mBluetooth.write(bytes);
            message = SettingsFragment.mBluetooth.incomingMessage;
            getSupportFragmentManager().beginTransaction().replace(R.id.container, go).commit();
//            message = SettingsFragment.mBluetooth.incomingMessage;
//            if(message.contentEquals("AOK"));
//              received = true;
//        }

    }

    public void go(View v){
        byte bytes[] = "@BTGO/".getBytes(Charset.defaultCharset());
        SettingsFragment.mBluetooth.write(bytes);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, home).commit();
    }

    public void speedUp(View v){
        byte bytes[] = "@BTSPD,1/".getBytes(Charset.defaultCharset());
//        Toast.makeText(this, "Increase Speed", Toast.LENGTH_SHORT).show();
        SettingsFragment.mBluetooth.write(bytes);
    }

    public void speedDown(View v){
        byte bytes[] = "@BTSPD,0/".getBytes(Charset.defaultCharset());
//        Toast.makeText(this, "Decrease Speed", Toast.LENGTH_SHORT).show();
        SettingsFragment.mBluetooth.write(bytes);
    }

    public void toMainActivity(View v) {
        //Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
        //getSupportFragmentManager().beginTransaction().remove(settings).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, home).commit();
//        run();
//        Intent i = new Intent(this, MainActivity.class);
//        startActivity(i);
//        setContentView(R.layout.activity_main);
//        if(fLayout!= null) {
//            fLayout.addView(tSurface); }
//        else {Toast.makeText(this, "NOPE", Toast.LENGTH_SHORT).show();}

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tSurface = new touchSurface(this);
        setContentView(R.layout.setup);
        fLayout = (FrameLayout) findViewById(R.id.frameLayout);//        if(fLayout!= null) {
//        fLayout.addView(tSurface); }
//        else {Toast.makeText(this, "NOPE", Toast.LENGTH_SHORT).show();}

        home = new HomeScreen(MainActivity.this);
        settings = new SettingsFragment(MainActivity.this);
        go = new GoFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, settings).commit();
        radius = tSurface.buffer;
//        speedIndicator.setText("num");//Integer.toString(SettingsFragment.speed));

        handler=new Handler();

        handler.postDelayed(myRunnable, 100);

//        String message = "";
//        while(true) {
//            String input = SettingsFragment.mBluetooth.incomingMessage;
//
//            if(message == input) {
//                Toast.makeText(this, input, Toast.LENGTH_SHORT).show();
//            }
//            message = input;
//            try {
//                wait();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        run();
    }

    @Override
    public void run() {
        message = SettingsFragment.mBluetooth.incomingMessage;
//        if(!message.isEmpty()) {
//            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
//        }

        //if(message.contains("e")) {
//            Toast.makeText(MainActivity.this, Integer.toString(SettingsFragment.speed), Toast.LENGTH_SHORT).show();
        //}
        handler.postDelayed(this, PERIOD);

    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}