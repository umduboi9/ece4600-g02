package carver25.dana.ece4600;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class SettingsFragment extends Fragment implements AdapterView.OnItemClickListener, Runnable{
    private static final String TAG = "MainActivity";

    Button btnBack;
    Switch swBTONOFF;
//    Button btnBTONOFF;
    Button btnDiscover;
//    Button btnDiscoverable;
    Button btnConnect;
    Button btnDisconnect;
    Button btnSpeedUp;
    Button btnSpeedDown;
//    Button btnPaired;
//    Button btnSend;
//    Button btnRead;
//    EditText text;
    ListView pairable;
//    ListView pairList;
    TextView paired, textView;
    SeekBar speedControl;
    Context mContext;
    BluetoothAdapter mBluetoothAdapter;
    static Bluetooth mBluetooth;
    Button btnEnableDisable_Discoverable;
    BluetoothDevice mBTDevice;
    ArrayList<String> pairArrayList;
    ArrayAdapter<String> arrayAdapterP;
    String message;
    private static final int PERIOD=5000;
    public static int speed;
    Runnable myRunnable;
    Handler handler;

    static UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    public ArrayList<BluetoothDevice> mBTDevices;// = new ArrayList<>();

    public DeviceListAdapter mDeviceListAdapter;

    public SettingsFragment(){}

    public SettingsFragment(Context context){
        mContext = context;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_settings, container, false);
        btnBack = v.findViewById(R.id.back);
        swBTONOFF = v.findViewById(R.id.swOn);
//        btnBTONOFF = v.findViewById(R.id.btnONOFF);
        btnDiscover = v.findViewById(R.id.btnFindUnpairedDevices);
//        btnDiscoverable = v.findViewById(R.id.btnDiscoverable_on_off);
        btnConnect = v.findViewById(R.id.connect);
//        btnDisconnect = v.findViewById(R.id.disconnect);
//        btnPaired = v.findViewById(R.id.pair);
//        btnSend = v.findViewById(R.id.send);
//        btnRead = v.findViewById(R.id.read);
        btnSpeedUp = v.findViewById(R.id.speedUp);
        btnSpeedDown = v.findViewById(R.id.speedDown);
//        text = v.findViewById(R.id.editText);
        pairable = (ListView) v.findViewById(R.id.lvNewDevices);
//        pairList = v.findViewById(R.id.pairList);
//        speedControl = v.findViewById(R.id.seekBar);
        textView = v.findViewById(R.id.textView);
        speed = 1;
        textView.setText(Integer.toString(speed));
        mBTDevices = new ArrayList<>();
//        final TextView paired = v.findViewById(R.id.paired);
        mBluetooth = new Bluetooth(mContext);
        //Broadcasts when bond state changes (ie:pairing)
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        getActivity().registerReceiver(mBroadcastReceiver4, filter);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        pairable.setOnItemClickListener(this);

        pairArrayList = new ArrayList<String>();
        arrayAdapterP = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.activity_settings, pairArrayList);
//        pairList.setAdapter(arrayAdapterP);

        handler=new Handler();

        handler.postDelayed(myRunnable, 100);

        swBTONOFF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableDisableBT();
            }
        });

//        btnBTONOFF.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //Log.d(TAG, "onClick: enabling/disabling bluetooth.");
//                enableDisableBT();
//            }
//        });

        btnDiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();
//
//                    //check BT permissions in manifest
                    checkBTPermissions();
//
                    mBluetoothAdapter.startDiscovery();
                    IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                    getActivity().registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
                }
                if (!mBluetoothAdapter.isDiscovering()) {
//
//                    //check BT permissions in manifest
                    checkBTPermissions();
//
                    mBluetoothAdapter.startDiscovery();
                    IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                    getActivity().registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);
                }
            }
        });

//        btnDiscoverable.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
//                discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
//                startActivity(discoverableIntent);
//
//                IntentFilter intentFilter = new IntentFilter(mBluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
//                getActivity().registerReceiver(mBroadcastReceiver2, intentFilter);
//            }
//        });

        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mBTDevice != null) {
                    startConnection();
                }
                else {
                    Toast.makeText(mContext,"ERROR: Could not connect", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        btnDisconnect.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                    mBluetooth.cancel();
//            }
//        });

//        btnPaired.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mBluetoothAdapter.isEnabled()) {
//                    paired.setText("Paired Devices");
//                    Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
//                    for (BluetoothDevice device : devices) {
//                        paired.append("\nDevice: " + device.getName() + "," + device);
//                    }
//                }
//            }
//        });

//        btnSend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    byte bytes[] = text.getText().toString().getBytes(Charset.defaultCharset());
//                    mBluetooth.write(bytes);
//                    text.clearComposingText();
//                }
//                catch (RuntimeException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getActivity(), "Start Bluetooth connection", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

//        btnRead.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                text.setText(mBluetooth.incomingMessage);
//            }
//        });

//        speedControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            @Override
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                speed = progress+1;
//                textView.setText(Integer.toString(speed));
//
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekBar seekBar) {
//
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekBar seekBar) {
////                mBluetooth.write((Integer.toString(speed)).getBytes(Charset.defaultCharset()));
//                boolean received = false;
//                String message;
////                while(!received){
//                    byte bytes[] = ("@BTSPD," + (char)(speed-16) + "/").getBytes(Charset.defaultCharset());
//                    mBluetooth.write(bytes);//.getBytes(Charset.defaultCharset()));                    message = SettingsFragment.mBluetooth.incomingMessage;
////                    if(message.contentEquals("AOK"));
////                    received = true;
////                }
//            }
//        });

        btnSpeedUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    byte bytes[] = "@BTSPD,1/".getBytes(Charset.defaultCharset());
                    mBluetooth.write(bytes);
                    speed = speed+1;
                    textView.setText(Integer.toString(speed));
                }
                catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }
        });

        btnSpeedDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    byte bytes[] = "@BTSPD,0/".getBytes(Charset.defaultCharset());
                    mBluetooth.write(bytes);
                    speed = speed-1;
                    textView.setText(Integer.toString(speed));
                }
                catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }
        });

        return v;
    }

    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (action.equals(mBluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, mBluetoothAdapter.ERROR);

                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        Log.d(TAG, "onReceive: STATE OFF");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING OFF");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE ON");
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING ON");
                        break;
                }
            }
        }
    };

    /**
     * Broadcast Receiver for changes made to bluetooth states such as:
     * 1) Discoverability mode on/off or expire.
     */
    private final BroadcastReceiver mBroadcastReceiver2 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED)) {

                int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.ERROR);

                switch (mode) {
                    //Device is in Discoverable Mode
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability Enabled.");
                        break;
                    //Device not in discoverable mode
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability Disabled. Able to receive connections.");
                        break;
                    case BluetoothAdapter.SCAN_MODE_NONE:
                        Log.d(TAG, "mBroadcastReceiver2: Discoverability Disabled. Not able to receive connections.");
                        break;
                    case BluetoothAdapter.STATE_CONNECTING:
                        Log.d(TAG, "mBroadcastReceiver2: Connecting....");
                        break;
                    case BluetoothAdapter.STATE_CONNECTED:
                        Log.d(TAG, "mBroadcastReceiver2: Connected.");
                        break;
                }

            }
        }
    };


    /**
     * Broadcast Receiver for listing devices that are not yet paired
     * -Executed by btnDiscover() method.
     */
    private BroadcastReceiver mBroadcastReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(TAG, "onReceive: ACTION FOUND.");

            if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if(!mBTDevices.contains(device) && device.getName() != null) {
                    mBTDevices.add(device);
                }
                Log.d(TAG, "onReceive: " + device.getName() + ": " + device.getAddress());
                mDeviceListAdapter = new DeviceListAdapter(context, R.layout.device_adapter_view, mBTDevices);
                pairable.setAdapter(mDeviceListAdapter);
            }
        }
    };

    /**
     * Broadcast Receiver that detects bond state changes (Pairing status changes)
     */
    private final BroadcastReceiver mBroadcastReceiver4 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                BluetoothDevice mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //3 cases:
                //case1: bonded already
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDED) {
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDED.");
                    mBTDevice = mDevice;
                }
                //case2: creating a bond
                if (mDevice.getBondState() == BluetoothDevice.BOND_BONDING) {
                    Log.d(TAG, "BroadcastReceiver: BOND_BONDING.");
                }
                //case3: breaking a bond
                if (mDevice.getBondState() == BluetoothDevice.BOND_NONE) {
                    Log.d(TAG, "BroadcastReceiver: BOND_NONE.");
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: called.");
        super.onDestroy();
        try {
            getActivity().unregisterReceiver(mBroadcastReceiver1);
        } catch (RuntimeException e) {
            e.getStackTrace();
        }
        try {
            getActivity().unregisterReceiver(mBroadcastReceiver2);
        } catch (RuntimeException e) {
            e.getStackTrace();
        }
        try {
            getActivity().unregisterReceiver(mBroadcastReceiver3);
        } catch (RuntimeException e) {
            e.getStackTrace();
        }
        try {
            getActivity().unregisterReceiver(mBroadcastReceiver4);
        } catch (RuntimeException e) {
            e.getStackTrace();
        }
//        getActivity().unregisterReceiver(mBroadcastReceiver2);
//        getActivity().unregisterReceiver(mBroadcastReceiver3);
//        getActivity().unregisterReceiver(mBroadcastReceiver4);
        //mBluetoothAdapter.cancelDiscovery();
    }

    public void startConnection() {
        startBTConnection(mBTDevice, uuid);
    }

    public void startBTConnection(BluetoothDevice device, UUID uuid) {
        mBluetooth.startClient(device, uuid);
    }


    public void enableDisableBT() {
        if (mBluetoothAdapter == null) {
            //Log.d(TAG, "enableDisableBT: Does not have BT capabilities.");
        }
        if (!mBluetoothAdapter.isEnabled()) {
            //Log.d(TAG, "enableDisableBT: enabling BT.");
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBTIntent);

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            getActivity().registerReceiver(mBroadcastReceiver1, BTIntent);
        }
        if (mBluetoothAdapter.isEnabled()) {
            //Log.d(TAG, "enableDisableBT: disabling BT.");
            mBluetoothAdapter.disable();

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            getActivity().registerReceiver(mBroadcastReceiver1, BTIntent);
        }

    }


    public void btnEnableDisable_Discoverable(View view) {
        Log.d(TAG, "btnEnableDisable_Discoverable: Making device discoverable for 300 seconds.");

        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);

        IntentFilter intentFilter = new IntentFilter(mBluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        getActivity().registerReceiver(mBroadcastReceiver2, intentFilter);

    }



    /**
     * This method is required for all devices running API23+
     * Android must programmatically check the permissions for bluetooth. Putting the proper permissions
     * in the manifest is not enough.
     * <p>
     * NOTE: This will only execute on versions > LOLLIPOP because it is not needed otherwise.
     */
    private void checkBTPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = this.getActivity().checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += this.getActivity().checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {

                this.requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        } else {
            Log.d(TAG, "checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //first cancel discovery because its very memory intensive.
        mBluetoothAdapter.cancelDiscovery();

        Log.d(TAG, "onItemClick: You Clicked on a device.");
        String deviceName = mBTDevices.get(i).getName();
        String deviceAddress = mBTDevices.get(i).getAddress();

        Log.d(TAG, "onItemClick: deviceName = " + deviceName);
        Log.d(TAG, "onItemClick: deviceAddress = " + deviceAddress);

        //create the bond.
        //NOTE: Requires API 17+? I think this is JellyBean
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            Log.d(TAG, "Trying to pair with " + deviceName);
            mBTDevices.get(i).createBond();
            mBTDevice = mBTDevices.get(i);
            //mBluetooth = new Bluetooth(SettingsActivity.this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mBluetoothAdapter.isEnabled()){
            swBTONOFF.setChecked(true);
        }
        else {
            swBTONOFF.setChecked(false);
        }
        run();
    }

    @Override
    public void run() {

        String message = SettingsFragment.mBluetooth.incomingMessage;
        textView.setText(Integer.toString(speed));
        //if(message.contains("e")) {
//        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
        //}
        //fLayout.postDelayed(, PERIOD);
//        handler.postDelayed(this, PERIOD);
    }
}
