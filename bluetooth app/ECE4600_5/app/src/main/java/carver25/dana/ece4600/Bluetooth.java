package carver25.dana.ece4600;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.UUID;

public class Bluetooth {
    private static final String Tag = "BluetoothConnection";
    private static final String appName = "myApp";
    private static final UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
    private static BluetoothAdapter mBluetoothAdapter;
    Context mContext;
    private AcceptThread mAcceptThread;
    private ConnectThread mConnectThread;
    private BluetoothDevice mmDevice;
    private UUID deviceUUID;
    ProgressDialog mProgressDialog;

    private ConnectedThread mConnectedThread;

    public static String incomingMessage;

    public Bluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        start();
    }

    public Bluetooth(Context context) {
        mContext = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        start();
    }

    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;
            try {
                tmp = mBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(appName, uuid);

                Log.d(Tag, "Accept Thread: Setting up server");
            } catch (IOException e) {
                Log.e(Tag, "AcceptThread: IOException" + e.getMessage());
            }
            mmServerSocket = tmp;
        }

        public void run() {
            Log.d(Tag, "AcceptThread: Running");
            BluetoothSocket socket = null;
            try {


                Log.d(Tag, "Server socket start...");

                socket = mmServerSocket.accept();

                Log.d(Tag, "Server socket accepted connection");
            } catch (IOException e) {
                Log.e(Tag, "AcceptThread: IOException" + e.getMessage());
            }

            if (socket != null) {
                //connected(socket,mmDevice);
            }

            Log.d(Tag, "AcceptThread ended");
        }

        public void cancel() {
            Log.d(Tag, "Cancelling AcceptThread");
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(Tag, "Close of AcceptThread failed" + e.getMessage());
            }
        }
    }

    private class ConnectThread extends Thread {
        private BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device, UUID uuid) {
            Log.d(Tag, "ConnectThread started");
            mmDevice = device;
            deviceUUID = uuid;
        }

        public void run() {
            BluetoothSocket tmp = null;
            Log.i(Tag, "Run mConnectThread");

            try {
                tmp = mmDevice.createRfcommSocketToServiceRecord(deviceUUID);
                Log.d(Tag, "ConnectThread: Trying to create socket");
            } catch (IOException e) {
                Log.e(Tag, "ConnectThread: Could not create socket" + e.getMessage());
            }
            mmSocket = tmp;

            mBluetoothAdapter.cancelDiscovery();
            try {
                mmSocket.connect();
                Log.d(Tag, "Run: ConnectThread connected");
            } catch (IOException e) {
                try {
                    mmSocket.close();
                    Log.d(Tag, "Run: Socket closed");
                } catch (IOException e1) {
                    Log.e(Tag, "Run: Unable to close connection in socket" + e1.getMessage());
                }
            }
            connected(mmSocket, mmDevice);
        }

        public void cancel() {
            try {
                Log.d(Tag, "Cancel: Closing Client socket");
                mmSocket.close();
            } catch (IOException e) {
                Log.e(Tag, "Cancel: close socket failed" + e.getMessage());
            }
        }
    }

    public synchronized void start() {
        Log.d(Tag, "Start");

        if(mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
        if (mAcceptThread == null) {
            mAcceptThread = new AcceptThread();
            mAcceptThread.start();
        }
    }

    public void startClient(BluetoothDevice device,UUID uuid) {
        Log.d(Tag, "StartClient: Started");
        mProgressDialog = ProgressDialog.show(mContext, "Connection Bluetooth",
                "Please Wait...", true);
        mConnectThread = new ConnectThread(device, uuid);
        mConnectThread.start();

    }
    private BluetoothSocket mmSocket;
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        public ConnectedThread(BluetoothSocket socket) {
            Log.d(Tag, "ConnectingThread: Starting");

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                mProgressDialog.dismiss();
            }
            catch (NullPointerException e) {
                e.printStackTrace();
            }
            try {
                tmpIn = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];
            int readBytes;
            while(true) {
                try {
                    readBytes = mmInStream.read(buffer);
                    incomingMessage = new String(buffer, 0, readBytes);
                    Log.d(Tag, "InputStream:" + incomingMessage);
                } catch (IOException e) {
                    Log.e(Tag, "Write: Error reading input stream" + e.getMessage());
                    break;
                }
            }
        }

//        Bundle bundle = new Bundle();
//        String line = mBufferedReader.readLine();
//        Log.d("message" , line );
//        bundle.putString(RemoteBluetooth.READ, line);
//        Message msg=mHandler.obtainMessage(RemoteBluetooth.MESSAGE_READ,line.length(),-1,buffer);
//        msg.setData(bundle);
//        mHandler.sendMessage(msg);
//    } catch (IOException e) {
//        Log.e(TAG, "disconnected", e);
//        connectionLost();
//        Log.e( TAG, "Could not connect to device", e );
//        close( mBufferedReader );
//        close( aReader );
//        break;

        public void write(byte[] bytes) {
            String text = new String(bytes, Charset.defaultCharset());
            Log.d(Tag, "Write: Writing to output stream" + text);
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) {
                Log.e(Tag, "Write: Error writing to output stream" + e.getMessage());
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {

            }
        }
    }

    private void connected(BluetoothSocket mmSocket, BluetoothDevice mmDevice) {
        Log.d(Tag, "Connected: Starting");
        mConnectedThread = new ConnectedThread(mmSocket);
        mConnectedThread.start();
    }

    public void write(byte[] out) {
        ConnectedThread r;
        Log.d(Tag, "Write: Write called");
        try {
            mConnectedThread.write(out);
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void cancel() {
        try{
            mmSocket.close();
        } catch (IOException e) {}
    }
}
